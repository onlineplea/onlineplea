package net.hmcts.atcm.onlineplea.selenium;

import net.hmcts.atcm.onlineplea.config.DriverFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;


public class DriverBase {

    private static List<DriverFactory> webDriverThreadPool = Collections.synchronizedList(new ArrayList<DriverFactory>());
    private static ThreadLocal<DriverFactory> driverFactory;

    @BeforeClass
    public static void instantiateDriverObject() {
     System.out.print("Instantiating driver");
        driverFactory = new ThreadLocal<DriverFactory>() {
            @Override
            protected DriverFactory initialValue() {
                DriverFactory driverFactory = new DriverFactory();
                webDriverThreadPool.add(driverFactory);
                return driverFactory;
            }
        };
    }

    public static WebDriver getDriver() throws Exception {
     if(driverFactory == null ) instantiateDriverObject();
        return driverFactory.get().getDriver();
    }

    @After
    public  void clearCookies() throws Exception {
          System.out.print("clearing cookies");

        getDriver().manage().deleteAllCookies();
    }

    @AfterClass
    public static void closeDriverObjects() {
          System.out.print("closeDriverObjects driver");

        for (DriverFactory driverFactory : webDriverThreadPool) {
            driverFactory.quitDriver();
        }
    }
}