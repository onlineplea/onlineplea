package net.hmcts.atcm.onlineplea.pagetests;

import net.hmcts.atcm.onlineplea.selenium.DriverBase;
import net.hmcts.atcm.onlineplea.selenium.ScreenShotRule;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MakePleaOnlineIT extends DriverBase {

@Rule
    public ScreenShotRule screenshotRule = new ScreenShotRule();
 
@Test
 public void shouldGetPageTitle() throws Exception {

  WebDriver driver = getDriver();

  driver.get("http://localhost:8080");

  Thread.sleep(5000);
  // Find the text input element by its name
  WebElement element = driver.findElement(By.className("lede"));

  // Check the title of the page
  System.out.println("Page title is: " + driver.getTitle());

  // Should see: "cheese! - Google Search"
  System.out.println("Element text is : " + element.getText());
 }

}
