/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hmcts.atcm.onlineplea.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author goutham
 */
public class FindYourCase {
 
 private static WebElement element = null;
 public static WebElement btn_Continue(WebDriver driver ){
 
  element = driver.findElement(By.id("continue"));
  
  return element;
 }
 
}
