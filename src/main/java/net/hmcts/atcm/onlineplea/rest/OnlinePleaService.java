package net.hmcts.atcm.onlineplea.rest;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.hmcts.atcm.onlineplea.data.Case;
import net.hmcts.atcm.onlineplea.data.Plea;
import net.hmcts.atcm.onlineplea.util.JsfUtil;
import net.hmcts.atcm.onlineplea.util.SslContextProvider;
import net.hmcts.atcm.onlineplea.util.TokenService;

/**
 *
 * @author betrand ugorji
 */
@RequestScoped
public class OnlinePleaService implements Serializable {

 @Inject
 private TokenService tokenService;

 /**
  * TODO Add token as request header
  * @param urn
  * @param postcode
  * @return Case Details
  */
 public Case findCaseByUrnAndPostcode(String urn, String postcode) {
  return client().target(JsfUtil.getResource("PLEA_API_PATH")).path("case")
          .queryParam("urn", urn).queryParam("postcode", postcode)
          .request(MediaType.APPLICATION_JSON_TYPE)
          .get(Response.class).readEntity(Case.class);
 }

 /**
  * TODO Add token as request header
  * @param plea
  * @return String Response
  */
 public String makeAPlea(Plea plea) {
  return client().target(JsfUtil.getResource("PLEA_API_PATH"))
          .path("plea").path("plead").path(tokenService.getServerToken())
          .request(MediaType.APPLICATION_JSON_TYPE)
          .post(Entity.entity(plea, MediaType.APPLICATION_JSON_TYPE))
          .readEntity(String.class);
 }

 private Client client() {
  if (JsfUtil.getResource("PLEA_API_PATH").contains("https")) {
   return ClientBuilder.newBuilder().sslContext(SslContextProvider.getSSLContext())
           .hostnameVerifier(SslContextProvider.getHostNameVerifier()).build();
  }
  return ClientBuilder.newClient();
 }

}
