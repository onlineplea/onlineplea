package net.hmcts.atcm.onlineplea.view;

import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import net.hmcts.atcm.onlineplea.util.JsfUtil;

/**
 *
 * @author betrand ugorji
 */
@Named("noticeOptionController")
@RequestScoped
public class NoticeOptionController {

 private List<String> items;
 private String selectedItem;

 @PostConstruct
 public void init() {
  items = Arrays.asList(
          JsfUtil.getResource("ThePoliceTrafficOffence"), 
          JsfUtil.getResource("TransportForLondon"));
 }

 public List<String> getItems() {
  return items;
 }

 public void setItems(List<String> items) {
  this.items = items;
 }

 public String getSelectedItem() {
  return selectedItem;
 }

 public void setSelectedItem(String selectedItem) {
  this.selectedItem = selectedItem;
 }
 
}
