package net.hmcts.atcm.onlineplea.view;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import net.hmcts.atcm.onlineplea.data.Case;
import net.hmcts.atcm.onlineplea.rest.OnlinePleaService;
import net.hmcts.atcm.onlineplea.util.JsfUtil;

/**
 *
 * @author Betrand Ugorji
 */
@SessionScoped
@Named("userSessionView")
public class UserSessionView implements Serializable {

 @Inject
 private OnlinePleaService pleaService;

 private String urn;
 private String postcode;
 private Case _case;
 private String previousPage;
 private String currentPage;

 /**
  * Creates a new instance of UserSessionView
  */
 public UserSessionView() {
 }

 @PostConstruct
 public void init() {
  _case = new Case();
 }

 /**
  *
  * @return find-your-case or enter-urn view
  */
 public String start() {
  String noticeOption = ((NoticeOptionController) JsfUtil
          .getBean("noticeOptionController")).getSelectedItem();

  if (noticeOption != null) {
   return noticeOption.equalsIgnoreCase(
           JsfUtil.getResource("ThePoliceTrafficOffence"))
           ? JsfUtil.getResource("EnterURN")
           : JsfUtil.getResource("FindYourCase");
  }
  return null;
 }

 /**
  *
  * @return find-your-case or enter-urn view
  */
 public String findCase() {
  _case = pleaService.findCaseByUrnAndPostcode(urn, postcode);
  return _case != null && _case.getCaseId() != null
          ? JsfUtil.getResource("YourDetails") : invalidCaseMessage();
 }

 private String invalidCaseMessage() {
  String errorMessage = JsfUtil.getResource("InvalidURNAndPostCode",
          new Object[]{urn, postcode});
  JsfUtil.displayErrorMessage(errorMessage);
  return "";
 }

 /**
  *
  * @return the previousPage - Navigate the previous page visited
  */
 public String back() {
  if (previousPage != null) {
   previousPage = previousPage.contains("?faces-redirect=true") ? previousPage
           : previousPage.equals("/") ? "/index.xhtml?faces-redirect=true"
           : !previousPage.contains(".xhtml") ? previousPage
           : previousPage + "?faces-redirect=true";
  }
  return previousPage;
 }

 //###--- ---------GETTERS AND SETTERS ----------------###
 public String getUrn() {
  return urn;
 }

 public void setUrn(String urn) {
  this.urn = urn;
 }

 public String getPostcode() {
  return postcode;
 }

 public void setPostcode(String postcode) {
  this.postcode = postcode;
 }

 public Case getCase() {
  return _case;
 }

 public void setCase(Case _case) {
  this._case = _case;
 }

 /**
  *
  * @return current page visited triggered from a hidden inputText
  */
 public String getCurrentPage() {
  String uri = JsfUtil.uri().replaceAll(JsfUtil.request().getContextPath(), "");
  if (uri != null && !uri.equals(currentPage) && JsfUtil.facesContext()
          .getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
   setPreviousPage(currentPage);
   setCurrentPage(uri);
  }
  System.out.println("CurrentPage:" + currentPage);
  System.out.println("PreviousPage:" + previousPage);
  return currentPage;
 }

 public void setCurrentPage(String currentPage) {
  this.currentPage = currentPage;
 }

 public void setPreviousPage(String currentPage) {
  this.previousPage = currentPage;
 }

 public String getPreviousPage() {
  return previousPage;
 }

}
