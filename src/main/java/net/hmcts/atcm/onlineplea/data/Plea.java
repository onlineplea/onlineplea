package net.hmcts.atcm.onlineplea.data;

/**
 *
 * @author betrand
 */
public class Plea {

    private int serialNumber;
    private Case _case;
    private String addressline1;
    private String addressline2;
    private String city;
    private boolean guilty;

    public Plea(Case caseRef, String addressline1, String addressline2,
            String city, boolean guilty) {
        this._case = caseRef;
        this.addressline1 = addressline1;
        this.addressline2 = addressline2;
        this.city = city;
        this.guilty = guilty;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Case getCase() {
        return _case;
    }

    public void setCase(Case _case) {
        this._case = _case;
    }

    public String getAddressline1() {
        return addressline1;
    }

    public void setAddressline1(String addressline1) {
        this.addressline1 = addressline1;
    }

    public String getAddressline2() {
        return addressline2;
    }

    public void setAddressline2(String addressline2) {
        this.addressline2 = addressline2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isGuilty() {
        return guilty;
    }

    public void setGuilty(boolean guilty) {
        this.guilty = guilty;
    }

}
