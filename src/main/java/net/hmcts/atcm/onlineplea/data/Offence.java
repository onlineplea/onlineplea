package net.hmcts.atcm.onlineplea.data;

/**
 *
 * @author betrand ugorji
 */
public class Offence {

 private String id;
 private String wording;

 public Offence() {
 }

 public String getId() {
  return id;
 }

 public void setId(String id) {
  this.id = id;
 }

 public String getWording() {
  return wording;
 }

 public void setWording(String wording) {
  this.wording = wording;
 }

}
