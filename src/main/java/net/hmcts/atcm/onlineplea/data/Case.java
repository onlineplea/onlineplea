package net.hmcts.atcm.onlineplea.data;

/**
 *
 * @author betrand ugorji
 */
public class Case {

 private String caseId;
 private Defendant defendant;
 private boolean processedOnline;

 public String getCaseId() {
  return caseId;
 }

 public void setCaseId(String caseId) {
  this.caseId = caseId;
 }

 public Defendant getDefendant() {
  return defendant;
 }

 public void setDefendant(Defendant defendant) {
  this.defendant = defendant;
 }

 public boolean isProcessedOnline() {
  return processedOnline;
 }

 public void setProcessedOnline(boolean processedOnline) {
  this.processedOnline = processedOnline;
 }

 @Override
 public String toString() {
  return "Defendant=" + defendant;
 }

}
