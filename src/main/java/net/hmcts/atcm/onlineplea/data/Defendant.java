package net.hmcts.atcm.onlineplea.data;

import java.util.List;

/**
 *
 * @author betrand ugorji
 */
public class Defendant {

 private String id;
 private String personId;
 private String firstName;
 private String lastName;
 private String homeTelephone;
 private String mobile;
 private String email;
 private String nationalInsurance;
 
private String dateOfBirth;
 private String nationality;
 private String disability;
 private String ethnicity;
 private String gender;
 private Address address;
 private List<Offence> offences;

 
 private String offenceTitle;
 private String offenceWording;
 
 private Boolean guilty;
 private Boolean courtPresence;
 
 private Interpreter interpreter;
 
 private WitnessInformation witness;
 
 public Defendant() {
 }

 public String getId() {
  return id;
 }

 public void setId(String id) {
  this.id = id;
 }

 public String getPersonId() {
  return personId;
 }

 public void setPersonId(String personId) {
  this.personId = personId;
 }

 public String getFirstName() {
  return firstName;
 }

 public void setFirstName(String firstName) {
  this.firstName = firstName;
 }

 public String getLastName() {
  return lastName;
 }

 public void setLastName(String lastName) {
  this.lastName = lastName;
 }

 public String getDateOfBirth() {
  return dateOfBirth;
 }

 public void setDateOfBirth(String dateOfBirth) {
  this.dateOfBirth = dateOfBirth;
 }

 public String getNationality() {
  return nationality;
 }

 public void setNationality(String nationality) {
  this.nationality = nationality;
 }

 public String getDisability() {
  return disability;
 }

 public void setDisability(String disability) {
  this.disability = disability;
 }

 public String getEthnicity() {
  return ethnicity;
 }

 public void setEthnicity(String ethnicity) {
  this.ethnicity = ethnicity;
 }

 public String getGender() {
  return gender;
 }

 public void setGender(String gender) {
  this.gender = gender;
 }

 public Address getAddress() {
  return address;
 }

 public void setAddress(Address address) {
  this.address = address;
 }

 public List<Offence> getOffences() {
  return offences;
 }

 public void setOffences(List<Offence> offences) {
  this.offences = offences;
 }

}
