package net.hmcts.atcm.onlineplea.data;

import java.util.Objects;

/**
 *
 * @author betrand
 */
public class Address {

 private String addressLine1;
 private String addressLine2;
 private String addressLine3;
 private String addressLine4;
 private String postcode;

 public Address() {
 }

 public String getAddressLine1() {
  return addressLine1;
 }

 public void setAddressLine1(String addressLine1) {
  this.addressLine1 = addressLine1;
 }

 public String getAddressLine2() {
  return addressLine2;
 }

 public void setAddressLine2(String addressLine2) {
  this.addressLine2 = addressLine2;
 }

 public String getAddressLine3() {
  return addressLine3;
 }

 public void setAddressLine3(String addressLine3) {
  this.addressLine3 = addressLine3;
 }

 public String getAddressLine4() {
  return addressLine4;
 }

 public void setAddressLine4(String addressLine4) {
  this.addressLine4 = addressLine4;
 }

 public String getPostcode() {
  return postcode;
 }

 public void setPostcode(String postcode) {
  this.postcode = postcode;
 }

 @Override
 public int hashCode() {
  int hash = 5;
  hash = 53 * hash + Objects.hashCode(this.addressLine1);
  hash = 53 * hash + Objects.hashCode(this.addressLine2);
  hash = 53 * hash + Objects.hashCode(this.addressLine3);
  hash = 53 * hash + Objects.hashCode(this.addressLine4);
  hash = 53 * hash + Objects.hashCode(this.postcode);
  return hash;
 }

 @Override
 public boolean equals(Object obj) {
  if (this == obj) {
   return true;
  }
  if (obj == null) {
   return false;
  }
  if (getClass() != obj.getClass()) {
   return false;
  }
  final Address other = (Address) obj;
  if (!Objects.equals(this.addressLine1, other.addressLine1)) {
   return false;
  }
  if (!Objects.equals(this.addressLine2, other.addressLine2)) {
   return false;
  }
  if (!Objects.equals(this.addressLine3, other.addressLine3)) {
   return false;
  }
  if (!Objects.equals(this.addressLine4, other.addressLine4)) {
   return false;
  }
  return Objects.equals(this.postcode, other.postcode);
 }

}
