package net.hmcts.atcm.onlineplea.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Betrand Ugorji
 */
public class JsfUtil {

 private static final Logger logger = Logger.getLogger(JsfUtil.class.getName());

 /**
  *
  * @param message to log
  */
 public static void log(String message) {
  logger.info(message);
 }

 /**
  *
  * @param key of the properties value
  * @return the value
  */
 public static String getResource(String key) {
  try {
   return ResourceBundle.getBundle("/Bundle").getString(key);
  } catch (Exception e) {
   logger.info(new StringBuilder()
           .append("JsfUtil getResource() Did not find ")
           .append(key).toString());
   return "";
  }
 }

 /**
  *
  * @param key of the properties value
  * @param params to be substituted in value from the properties file
  * @return new String value outcome after substitution
  */
 public static String getResource(String key, Object... params) {
  try {
   return MessageFormat.format(ResourceBundle
           .getBundle("/Bundle").getString(key), params);
  } catch (MissingResourceException e) {
   logger.info(new StringBuilder()
           .append("JsfUtil getResource() Did not find ")
           .append(key).toString());
   return '!' + key + '!';
  }
 }

 /**
  *
  * @param bean a - Bean Name usually starts with lowercase
  * @return the Bean reference if found
  */
 public static Object getBean(String bean) {
  return facesContext().getApplication().getELResolver()
          .getValue(facesContext().getELContext(), null, bean);
 }

 /**
  *
  * @param url navigate to a valid url
  */
 public static void navigateTo(String url) {
  logger.info(new StringBuilder()
          .append("navigateTo url: ")
          .append(url).toString());
  try {
   FacesContext.getCurrentInstance().getApplication().getNavigationHandler()
           .handleNavigation(FacesContext.getCurrentInstance(), null, url);
  } catch (Exception e) {
   logger.info(new StringBuilder()
           .append("JsfUtil navigateTo() to url ")
           .append(url).toString());
  }
 }

 public static void displayErrorMessage(String message) {
  FacesMessage facesMessage = new FacesMessage(
          FacesMessage.SEVERITY_ERROR, message, message);
  facesContext().addMessage(null, facesMessage);
 }

 /**
  *
  * @return only the contextPath e.g. /onlineplea
  */
 public static String path() {
  return ((HttpServletRequest) request()).getContextPath();
 }

 /**
  *
  * @return only the ServletPath e.g. /onlineplea/your-details you need to add
  * "?faces-redirect=true" for redirects on the client
  */
 public static String page() {
  return ((HttpServletRequest) request()).getServletPath();
 }

 /**
  *
  * @return the full path such as /onlineplea/plea/enter_urn.xhtml
  */
 public static String uri() {
  return ((HttpServletRequest) request()).getRequestURI();
 }

 public static HttpServletRequest request() {
  return (HttpServletRequest) externalContext().getRequest();
 }

 public static HttpServletResponse response() {
  return (HttpServletResponse) externalContext().getResponse();
 }

 public static ServletContext servletContext() {
  return (ServletContext) externalContext().getContext();
 }

 public static ExternalContext externalContext() {
  return facesContext().getExternalContext();
 }

 public static FacesContext facesContext() {
  return FacesContext.getCurrentInstance();
 }

}
