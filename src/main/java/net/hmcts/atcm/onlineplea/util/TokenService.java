package net.hmcts.atcm.onlineplea.util;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author betrand ugorji
 */
@ApplicationScoped
public class TokenService {

 private static String token = null;

 @Resource
 private ManagedScheduledExecutorService scheduler;

 /**
  * Renew Token e.g. every 14 minutes
  */
 @PostConstruct
 public void init() {
  this.scheduler.scheduleAtFixedRate(this::renewToken, 0, Long.parseLong(
          JsfUtil.getResource("TIME_OUT_IN_SECONDS")), TimeUnit.SECONDS);
 }

 /**
  * Renew token on demand
  */
 public void renewToken() {
  token = client().target(JsfUtil.getResource("TOKEN_API_PATH"))
          .path("token").request(MediaType.TEXT_PLAIN).get(String.class);
  Logger.getLogger(TokenService.class.getName())
          .log(Level.INFO, "@TokenService Renewed Token {0} ", token);
 }

 /**
  *
  * @return Current Request token
  */
 public String getServerToken() {
  return token;
 }

 private Client client() {
  if (JsfUtil.getResource("TOKEN_API_PATH").contains("https")) {
   return ClientBuilder.newBuilder().sslContext(SslContextProvider.getSSLContext())
           .hostnameVerifier(SslContextProvider.getHostNameVerifier()).build();
  }
  return ClientBuilder.newClient();
 }

}
