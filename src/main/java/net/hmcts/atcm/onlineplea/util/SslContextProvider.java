package net.hmcts.atcm.onlineplea.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 *
 * @author betrand ugorji
 */
public class SslContextProvider {
 
 public static HostnameVerifier getHostNameVerifier() {
  return (String hostname, javax.net.ssl.SSLSession sslSession) -> true;
 }

 public static SSLContext getSSLContext() {
  SSLContext context = null;
  try {
   context = SSLContext.getInstance("SSL");
   context.init(null, new TrustManager[]{new X509TrustManager() {
    @Override
    public void checkClientTrusted(
            java.security.cert.X509Certificate[] xcs, String string)
            throws CertificateException {
    }

    @Override
    public void checkServerTrusted(
            java.security.cert.X509Certificate[] xcs, String string)
            throws CertificateException {
    }

    @Override
    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
     return new java.security.cert.X509Certificate[0];
    }
   }
   }, new SecureRandom());
  } catch (NoSuchAlgorithmException | KeyManagementException e) {
   Logger.getLogger(TokenService.class.getName())
           .log(Level.INFO, "@TokenService getSSLContext {0}", e);
  }
  return context;
 }

}
