const gulp = require('gulp')
const gutil = require('gulp-util')
const requireDir = require('require-dir')
const runsequence = require('run-sequence')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')


// Split gulp tasks ------------------------------------
// Allows for multiple file gulp tasks.
// -----------------------------------------------------


const clean = require('del')


// Clean -----------------------------------------------
// Deletes public folder.
// -----------------------------------------------------

gulp.task('clean', () => {
    return clean([
        'src/main/webapp/resources/css/'
    ])
})


// Build  ----------------------------------------------
// 1. Clean public folder
// 2. Compiles CSS from SASS
// 3. Render nunjucks templates
// 4. Minifies images with caching
// 5. ESLint our javascripts
// 6. Minify javascript
// 5. Validates HTML
// -----------------------------------------------------

gulp.task('build', () => {
    runsequence('clean', 
        'sass' )
})

//gulp.task('build', () => {
//    runsequence('clean', [
//        'sass'
//], 'html')
//})


// Develop ---------------------------------------------
// Run server and watch for changes.
// -----------------------------------------------------
//
//gulp.task('develop', [
//    'watch'
//])


// Watch -----------------------------------------------
// 1. Sass
// 2. Nunjucks
// 3. Images
// 4. Javascripts
// -----------------------------------------------------

gulp.task('watch', () => {
    runsequence(
        'clean',
    'watch:sass')
})

gulp.task('sass', () => {
    runsequence(
//        'clean',
    'sass:sass')
})




gulp.task('watch:sass', () => {
    return gulp.watch('src/main/webapp/resources/sass/**/*.scss', ['sass'])
})




// Default ---------------------------------------------
// Output list of gulp tasks.
// -----------------------------------------------------

gulp.task('default',[ 'sass' ])

gulp.task('sass:sass', () => {
    return gulp.src('src/main/webapp/resources/sass/**/*.scss')
        .pipe(sass({
            includePaths: [
                './node_modules/govuk_frontend_toolkit/stylesheets',
                './node_modules/govuk-elements-sass/public/sass'
            ],
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest('src/main/webapp/resources/css'))
})

